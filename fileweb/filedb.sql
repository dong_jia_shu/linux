-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2021-12-08 14:39:45
-- 服务器版本： 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `filedb`
--

-- --------------------------------------------------------

--
-- 表的结构 `t_department`
--

CREATE TABLE `t_department` (
  `id` int(4) NOT NULL,
  `departmentname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_department`
--

INSERT INTO `t_department` (`id`, `departmentname`) VALUES
(1, '一班'),
(2, '二班');

-- --------------------------------------------------------

--
-- 表的结构 `t_file`
--

CREATE TABLE `t_file` (
  `id` int(4) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `inputdate` varchar(50) DEFAULT NULL,
  `fromuser` int(4) DEFAULT NULL,
  `touser` int(4) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `typeid` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_file`
--

INSERT INTO `t_file` (`id`, `title`, `remark`, `url`, `inputdate`, `fromuser`, `touser`, `status`, `typeid`) VALUES
(10, '20191313', '20191313', '2021-12-08-21-11-05F.doc', '2021-12-08 21:11:08', 8, 3, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `t_filetype`
--

CREATE TABLE `t_filetype` (
  `id` int(4) NOT NULL,
  `typename` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_filetype`
--

INSERT INTO `t_filetype` (`id`, `typename`) VALUES
(1, '加密'),
(2, '普通');

-- --------------------------------------------------------

--
-- 表的结构 `t_user`
--

CREATE TABLE `t_user` (
  `id` int(4) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `departmentid` int(4) DEFAULT NULL,
  `zhiwei` varchar(50) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `zuzhiid` int(4) DEFAULT NULL,
  `sex` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_user`
--

INSERT INTO `t_user` (`id`, `username`, `password`, `realname`, `role`, `departmentid`, `zhiwei`, `tel`, `email`, `zuzhiid`, `sex`) VALUES
(1, 'admin', '1', '管理员', '0', NULL, '', '1881022222222', 'admin@163.com', NULL, '男'),
(2, '20191313', '1', '戴君熹', '1', 1, '初级职员', '18810603148', 'djx12138@qq.com', 1, '男'),
(3, '20191327', '1', '唐子越', '2', 1, '初级职员', '1360502212233', 'tzy12138@qq.com', 1, '男'),
(4, '20191309', '1', '刘嘉祺', '2', 1, '初级职员', '16632232244', 'ljq12138@qq.com', 2, '男'),
(5, '20191302', '1', '董佳帅', '1', 2, '初级职员', '1881022222222', 'djs12138@qq.com', 2, '男'),
(6, '20191307', '1', '王竞锐', '1', 2, '初级职员', '12155222025', 'wjr12138@qq.com', 3, '男'),
(7, 'admin20191313', '1', '管理员戴君熹', '0', 2, '初级职员', '18810603148', '627994942@qq.com', 3, '男'),
(8, 'yuangong20191313', '1', '员工戴君熹', '2', 1, '初级职员', '16632232244', '12138@qq.com', 4, '男');

-- --------------------------------------------------------

--
-- 表的结构 `t_zuzhi`
--

CREATE TABLE `t_zuzhi` (
  `id` int(4) NOT NULL,
  `zuzhiname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_zuzhi`
--

INSERT INTO `t_zuzhi` (`id`, `zuzhiname`) VALUES
(1, '密码系'),
(2, '网空系'),
(3, '电通系'),
(4, '管理系');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_department`
--
ALTER TABLE `t_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_file`
--
ALTER TABLE `t_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_filetype`
--
ALTER TABLE `t_filetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_zuzhi`
--
ALTER TABLE `t_zuzhi`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `t_department`
--
ALTER TABLE `t_department`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `t_file`
--
ALTER TABLE `t_file`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `t_filetype`
--
ALTER TABLE `t_filetype`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `t_zuzhi`
--
ALTER TABLE `t_zuzhi`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
