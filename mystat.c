#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    struct stat sb;
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s &lt;pathname&gt;\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (stat(argv[1],&sb) == -1)
    {
        perror("stat");
        exit(EXIT_FAILURE);
    }
    printf("File type:                ");
    switch (sb.st_mode&S_IFMT)
    {
    case S_IFBLK:
        printf("block device\n");
        break;
    case S_IFCHR:
        printf("character device\n");
        break;
    case S_IFDIR:
        printf("directory\n");
        break;
    case S_IFIFO:
        printf("FIFO/pipe\n");
        break;
    case S_IFLNK:
        printf("symlink\n");
        break;
    case S_IFREG:
        printf("regular file\n");
        break;
    case S_IFSOCK:
        printf("socket\n");
        break;
    default:
        printf("unknown?\n");
        break;
    }
    printf("文件: '%s'\n",argv[1]);
    printf("大小: %lld      ",(long long) sb.st_size);
    printf("块: %lld               ",(long long) sb.st_blocks);
    printf("IO块: %ld\n",(long) sb.st_blksize);
    printf("设备: %d      ",sb.st_dev);//文件设备编号
    printf("Inode: %d      ",sb.st_ino);//文件ｉ节点标号
    printf("硬链接: %ld\n", (long) sb.st_nlink);
    printf("权限: %lo (octal)      ",(unsigned long) sb.st_mode);
    printf("Uid=%ld       Gid=%ld\n",(long) sb.st_uid, (long) sb.st_gid);
    printf("最近更改: %s", ctime(&sb.st_ctime));
    printf("最近访问: %s", ctime(&sb.st_atime));
    printf("最近改动: %s", ctime(&sb.st_mtime));
    printf("创建时间: -\n");
    exit(EXIT_SUCCESS);
}